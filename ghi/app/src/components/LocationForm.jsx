import React, { useEffect, useState } from "react";

function LocationForm() {

  // Set the useState hook to store "name" in the component's state,
  // with a default initial value of an empty string.
  const [states, setStates] = useState([]);
  const [name, setName] = useState('');
  const [roomCount, setRoom] = useState(0);
  const [city, setCity] = useState('');
  const [state, setState] = useState('');


  // Bring in (location) state data to access properties
  const fetchData = async () => {
    const url = "http://localhost:8000/api/states/";

    try {
        const response = await fetch(url);
      // console.log(response)
      if (response.ok) {
        const data = await response.json();
        // now we bind the state method we created above to the data we are pulling in:
        setStates(data.states);
      }
  } catch (error) {
    console.error(error);
  }
}

  // Create the handleNameChange method to take what the user inputs
  // into the form and store it in the state's "name" variable.
  const handleNameChange = (event) => {
    // The event parameter is the event that occurred
    // The target property is the HTML tag that caused the event
    // event.target is the user's input in the form for the location's name
    const value = event.target.value;
    setName(value);
  }

  const handleRoomCountChange = (event) => {
    const value = event.target.value;
    setRoom(value);
  }

  const handleCityChange = (event) => {
    const value = event.target.value;
    setCity(value);
  }

  const handleStateChange = (event) => {
    const value = event.target.value;
    setState(value);
  }

    // Let's handle the Submit button now to save the state of the data stored in the variables
    const handleSubmit = async (event) => {
      event.preventDefault();

      const data = {}

      data.room_count = roomCount;
      data.name = name;
      data.city = city;
      data.state = state;

      const locationUrl = 'http://localhost:8000/api/locations/';
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };

      const response = await fetch(locationUrl, fetchConfig);
      if (response.ok) {
        const newLocation = await response.json();
        console.log(newLocation);
        // once it saves the data, reset the form
        setName('');
        setRoom('');
        setCity('');
        setState('');
  }
}

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new location</h1>
          <form onSubmit={ handleSubmit } id="create-location-form">
            <div className="form-floating mb-3">
              <input onChange={ handleNameChange }
                placeholder="Name"
                required
                type="text"
                name="name"
                value={ name }
                id="name"
                className="form-control"
              />
              <label htmlFor="name">Name</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={ handleRoomCountChange }
                placeholder="Room count"
                required
                type="number"
                name="room_count"
                value={ roomCount }
                id="room_count"
                className="form-control"
              />
              <label htmlFor="room_count">Room count</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={ handleCityChange }
                placeholder="City"
                required
                type="text"
                name="city"
                id="city"
                value={ city }
                className="form-control"
              />
              <label htmlFor="city">City</label>
            </div>

            <div className="mb-3">
              <select onChange={ handleStateChange }
                required name="state"
                value={ state }
                id="state"
                className="form-select">
                  <option value="">Choose a state</option>
                  {states.map((state) => {
                    return (
                      <option key={state.abbreviation} value={state.abbreviation}>
                        {state.name}
                      </option>
                    );
                    })}
              </select>
            </div>

            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default LocationForm
