import React from "react";
import Nav from "./components/Nav";
import AttendeesList from "./components/AttendeesList";
import LocationForm from "./components/LocationForm";
import ConferenceForm from "./components/ConferenceForm";
import AttendConferenceForm from "./components/AttendConferenceForm";
import PresentationForm from "./components/PresentationForm";
import MainPage from "./components/MainPage";
import { BrowserRouter, Route, Routes } from 'react-router-dom';

function App({ attendees }) {
  if (attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="conferences/new" element={<ConferenceForm />} />
          <Route path="attendees/new" element={<AttendConferenceForm />} />
          <Route path="locations/new" element={<LocationForm />} />
          <Route path="attendees" element={<AttendeesList attendees={attendees} />} />
          <Route path="presentations/new" element={<PresentationForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
